This repository contains data from channel flow simulations performed in OpenFOAM and discussed in a techincal report found at

http://uu.diva-portal.org/smash/record.jsf?pid=diva2%3A812934&dswid=8423

If you use this data, don't forget to cite the report! :)

For questions or remarks please contact:

*timofey dot mukha at it dot uu dot se*

*mattias dot liefvendahl at it dot uu dot se*

The repository consists of the following:

- The **profiles** directory, which contains all the data. Each files begins with M#, which denotes the simulation/grid, see the report for details. The rest of the name depends on the contents of the file. The following list names all the provided profiles and the associated file names:
    * Mean streamwise velocity  -- _mean\_u_.
    * Reynolds stresses -- _Reynolds\_stresses_
    * Skewness factors -- _S_
    * Flatness factors -- _F_
    * Vorticity fluctuatons -- _vorticity\_fluctuations_
    * Two-point autocorellations in space along _x_  -- _autocorr\_x_ 
    * Two-point autocorellations in space along _z_ -- _autocorr\_z_

  Any additional information may be found inside individual files.

- The **OpenFOAM\_setup** directory, which contains OpenFOAM setup files, which can be used to setup a simulation that would reproduce the data. Fields that can be used as initial conditions are also provided. Please note that these files were used with the version 2.2.0 of the software. In the latest versions quite a few things have changed, in particular the set-up of the turbulence modelling and the name of the fvOption used to drive the flow. Those using latest OpenFOAM versions are encouraged to look at the channel395 tutorial shipped with the software to see how the things are set up now, it should be trivial to apply the changes to the cases provided in the repo.

  **UPDATE** There have been issues with the skewness* and kurtosis* function objects reported from users of OpenFOAM version 2.3.0 and above. The best solution is to remove these function objects all together. Skewness and kurtosis can be computed as a post-processing step using the saved mean values of powers of the velocity field!

  **UPDATE 2** I have now removed the skewness and kurtosis function objects from the controlDict. Of course, you can check out an older commit if you want the back :).


Note that the **controlDict** assumes that you have **swak4foam** installed.

- Maintenance script **check\_data.py** which performs a check on the data files, to see that they are loadable and not corrupted.

In order to average data along x and z we recommend using the following utility:

https://bitbucket.org/lesituu/postchannelflow

It is basically an enhanced version of the default OpenFOAM utility postChannel, it allows you to choose what fields you want to average.